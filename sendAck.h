
#ifndef SENDACK_H
#define SENDACK_H

//payload of the msg
typedef nx_struct my_msg {
	//field 1
	nx_uint16_t value;
	//field 2
	nx_uint8_t type;
	//field 3
	nx_uint16_t counter;
} my_msg_t;

#define REQ 1
#define RESP 2

enum{
AM_MY_MSG = 6,
};

#endif
