#include "DATA/TEMP_DATA.h"

generic module fakeSensorP() {

	provides interface Read<uint16_t> as TempRead;

	uses interface Timer<TMilli> as TimerReadTemp;
	uses interface Random;
	uses interface ParameterInit<uint16_t> as SeedInit;

} implementation {

	uint16_t temp_index = 0;

	uint16_t temp_read_value = 0;


	//***************** Read interface ********************//
	command error_t TempRead.read(){
		
		uint16_t tmp = call TimerReadTemp.getNow();
		call SeedInit.init(tmp);
		
		temp_index = (call Random.rand16()%671);
		
		temp_read_value = (TEMP_DATA[temp_index]-32)/1.8;
		temp_index++;
		if(temp_index==TEMP_DATA_SIZE)
			temp_index = 0;

		call TimerReadTemp.startOneShot( 2 );
		return SUCCESS;
	}

	//***************** Timer interfaces ********************//
	event void TimerReadTemp.fired() {
		signal TempRead.readDone( SUCCESS, temp_read_value );
	}

}
