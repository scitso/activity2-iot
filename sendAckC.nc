
#include "sendAck.h"
#include "Timer.h"

module sendAckC{

  uses {
  /****** INTERFACES *****/
	interface Boot;

  //interfaces for communication
  interface SplitControl;
	interface Packet;
  interface AMSend;
	interface Receive;
  interface PacketAcknowledgements;
	//interface for timer
	interface Timer<TMilli> as MilliTimer;
    //other interfaces, if needed

	//interface used to perform sensor reading (to get the value from a sensor)
	interface Read<uint16_t> as TempRead;
  }

} implementation {

  uint8_t counter=0;
  uint8_t rec_id;
  message_t packet;

  void sendReq();
  void sendResp();


  //***************** Send request function ********************//
  void sendReq() {
	/* This function is called when we want to send a request
	 *
	 * STEPS:
	 * 1. Prepare the msg
	 * 2. Set the ACK flag for the message using the PacketAcknowledgements interface
	 *     (read the docs)
	 * 3. Send an UNICAST message to the correct node
	 * X. Use debug statements showing what's happening (i.e. message fields)
	 */

	my_msg_t* msg = (my_msg_t*)(call Packet.getPayload(&packet, sizeof(my_msg_t)));
  if(msg == NULL) return ;
  msg -> type = REQ;
  msg -> counter = counter;
  if(call PacketAcknowledgements.requestAck(&packet)== SUCCESS){
    dbg("radio_pack", "Preparing message that need ack... \n");

    if(call AMSend.send(2, &packet,sizeof(my_msg_t)) == SUCCESS){
       dbg("radio_send", "Packet passed to lower layer successfully!\n");
       dbg("radio_pack",">>>Pack\n \t Payload length %hhu \n", call Packet.payloadLength( &packet ) );
       dbg_clear("radio_pack","\t Payload Sent\n" );
       dbg_clear("radio_pack", "\t\t type: %hhu \n ", msg->type);
       dbg_clear("radio_pack", "\t\t counter: %hhu \n", msg->counter);

      }
    }
  counter++;
  return;
  }


  //****************** Task send response *****************//
  void sendResp() {
  	/* This function is called when we receive the REQ message.
  	 * Nothing to do here.
  	 * `call Read.read()` reads from the fake sensor.
  	 * When the reading is done it raise the event read one.
  	 */
	call TempRead.read();
  }

  //***************** Boot interface ********************//
  event void Boot.booted() {
	dbg("boot","Application booted at time %s \n", sim_time_string());
	/* Fill it ... */
  call SplitControl.start();
  }

  //***************** SplitControl interface ********************//
  event void SplitControl.startDone(error_t err){
    /* Fill it ... */
    if(err == SUCCESS){
      dbg("radio", "Radio on\n");
      if(TOS_NODE_ID == 1){
        call MilliTimer.startPeriodic(1000);
      }
    }
    else{
      call SplitControl.start();
    }
  }

  event void SplitControl.stopDone(error_t err){
    /* Fill it ... */
  }

  //***************** MilliTimer interface ********************//
  event void MilliTimer.fired() {
	/* This event is triggered every time the timer fires.
	 * When the timer fires, we send a request
	 * Fill this part...
	 */
   if(TOS_NODE_ID == 1) sendReq();
  }


  //********************* AMSend interface ****************//
  event void AMSend.sendDone(message_t* buf,error_t err) {
	/* This event is triggered when a message is sent
	 *
	 * STEPS:
	 * 1. Check if the packet is sent
	 * 2. Check if the ACK is received (read the docs)
	 * 2a. If yes, stop the timer. The program is done
	 * 2b. Otherwise, send again the request
	 * X. Use debug statements showing what's happening (i.e. message fields)
	 */

   if(&packet == buf && err == SUCCESS){
     dbg("radio_send", "Packet sent...");
     dbg_clear("radio_send", " at time %s \n", sim_time_string());
     if(!(call PacketAcknowledgements.wasAcked(&packet))) {
       dbg("radio_send", "Packet NOT Acked yet...   \n");
       dbg("radio_send", "keeping the timer on %s \n", sim_time_string());
        //call MilliTimer.startPeriodic(1000);
     }
     else{
		 dbg("radio_send", "Packet Acked!\n");
		 dbg_clear("radio_send", "Stopping the timer\n");
		 call MilliTimer.stop();
     }
   }
  }

  //***************************** Receive interface *****************//
  event message_t* Receive.receive(message_t* buf,void* payload, uint8_t len) {
	/* This event is triggered when a message is received
	 *
	 * STEPS:
	 * 1. Read the content of the message
	 * 2. Check if the type is request (REQ)
	 * 3. If a request is received, send the response
	 * X. Use debug statements showing what's happening (i.e. message fields)
	 */

   if( len != sizeof(my_msg_t)) return buf;
   else{

     my_msg_t* msg = (my_msg_t*)payload;

	 dbg("radio_rec", "Received packet at time %s\n", sim_time_string());
	 dbg("radio_pack"," Payload length %hhu \n", call Packet.payloadLength( buf ));
	 dbg("radio_pack", ">>>Pack Received\n");
	 dbg_clear("radio_pack","\t\t Payload Received\n" );
	 dbg_clear("radio_pack", "\t\t type: %hhu \n ", msg->type);
	 dbg_clear("radio_pack", "\t\t data: %hhu \n", msg->value);
	 dbg_clear("radio_pack", "\t\t counter: %hhu \n", msg->counter);

     if(TOS_NODE_ID == 2 && msg->type == REQ){
       counter = msg -> counter;
       sendResp();
     }
     else{
       dbg("radio_rec","Printed the response\n");
     }

     return buf;
   }
  }

  //************************* Read interface **********************//
  event void TempRead.readDone(error_t result, uint16_t data) {
	/* This event is triggered when the fake sensor finish to read (after a Read.read())
	 *
	 * STEPS:
	 * 1. Prepare the response (RESP)
	 * 2. Send back (with a unicast message) the response
	 * X. Use debug statement showing what's happening (i.e. message fields)
	 */
    my_msg_t* msg = (my_msg_t*)(call Packet.getPayload(&packet, sizeof(my_msg_t)));
    if(msg == NULL) return;
    msg -> type = RESP;
    msg -> value = data;
    msg -> counter = counter+1;
    call PacketAcknowledgements.requestAck(&packet);
    dbg("radio_resp", "Preparing response message... \n");

    if(call AMSend.send(1, &packet, sizeof(my_msg_t)) == SUCCESS){
      dbg("radio_send", "Packet passed to lower layer successfully!\n");
      dbg("radio_pack",">>>Pack\n \t Payload length %hhu \n", call Packet.payloadLength( &packet ) );
      dbg_clear("radio_pack","\t Payload Sent\n" );
      dbg_clear("radio_pack", "\t\t type: %hhu \n ", msg->type);
      dbg_clear("radio_pack", "\t\t temp: %hhu \n", msg->value);
      dbg_clear("radio_pack", "\t\t counter: %hhu \n", msg->counter);
    }

  }
}
