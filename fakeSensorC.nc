
generic configuration FakeSensorC() {

	provides interface Read<uint16_t> as TempRead;

} implementation {

	components MainC;
	components new fakeSensorP();
	components new TimerMilliC() as ReadTempTimer;
	components RandomC;

	//Connects the provided interface
	TempRead = fakeSensorP.TempRead;

	//Timer interface
	fakeSensorP.TimerReadTemp -> ReadTempTimer;
	
	//Random part
	fakeSensorP.Random -> RandomC;
	fakeSensorP.SeedInit -> RandomC;
}
